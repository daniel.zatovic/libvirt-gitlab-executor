# machine.py - module containing VM abstraction class
#
# Copyright (C) 2021 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os
import shlex
import subprocess
import yaml

from tempfile import NamedTemporaryFile

from provisioner import cloud_init
from provisioner.configmap import ConfigMap
from provisioner.libvirt_handle import LibvirtHandle
from provisioner.ssh import SSHConn
from provisioner.snapshot_manager import SnapshotManager
from provisioner.util import check_commands_present, get_command_output, run_command, ssh_wait


log = logging.getLogger(__name__)


class Machine:
    """
    This class serves as a simple abstraction over a virtual machine.

    The prerequisite to provisioning a new VM is that a base image of the
    specific distribution already exists in the libvirt land.

    The usual flow of actions is as follow:
        m = Machine(name)
        m.provision(distro)
        conn = m.connect(ssh_key_file)  # verifies that jobs can be sent to the
                                          VM over SSH
        conn.upload(script, remote_dest)
        rc = conn.exec(cmdlinestr)
    """

    @property
    def conn(self):
        if self._conn is None:
            self._conn = SSHConn(self.name)
        return self._conn

    def __init__(self, name, config=None):
        self.name = name
        self._conn = None

        if config is None:
            self.config = ConfigMap()
        else:
            self.config = config

        required_commands = []
        if self.config["generate_cloud_init_iso"]:
            required_commands.append("genisoimage")
        if (self.config["ci_job_token"] is not None and
                self.config["gitlab"]["upload_console"]):
            required_commands.append("gitlab-runner")
        if "vendor_id" in self.config and "device_id" in self.config and \
           self.config["vendor_id"] and self.config["device_id"]:
            required_commands.append("lspci")

        check_commands_present(required_commands)

    def connect(self, ssh_key_path, ssh_user):
        """
        Opens an SSH channel to the VM.

        :param ssh_key_path: path to the SSH key to be used (as string)
        """

        if ssh_key_path is None:
            raise ValueError(f"Failed to connect to {self.name}: "
                             "No SSH key specified")

        self.conn.connect(key_filepath=ssh_key_path,
                          username=ssh_user)

    def _dump_meta_data(self, meta_data):
        tempfile = NamedTemporaryFile(delete=False,
                                      prefix=(self.name + "-cloud-config-meta-data"))

        with open(tempfile.name, "w") as fd:
            # nasty hack to force PyYAML not to break long lines by default
            from math import inf

            # write the header first, otherwise cloud-init will ignore the file
            fd.write(yaml.dump(meta_data, width=inf))
        return tempfile.name

    def _dump_user_data(self, user_data):
        tempfile = NamedTemporaryFile(delete=False,
                                      prefix=(self.name + "-cloud-config-user-data"))

        with open(tempfile.name, "w") as fd:
            # nasty hack to force PyYAML not to break long lines by default
            from math import inf

            # write the header first, otherwise cloud-init will ignore the file
            fd.write("#cloud-config\n")
            fd.write(yaml.dump(user_data, width=inf))
        return tempfile.name

    def _delete_cloud_init_iso(self):
        cloud_init_iso = f"/tmp/{self.name}-cloud-init.iso"

        try:
            os.unlink(cloud_init_iso)
        except FileNotFoundError:
            pass

    def find_pci_address(self):
        cmd = ["lspci", "-m", "-nn", "-d",
               f"{self.config['vendor_id']}:{self.config['device_id']}"]
        output = shlex.split(get_command_output(cmd))

        if len(output) == 0:
            raise Exception(f"Cannot find PCIe device {self.config['vendor_id']}:{self.config['device_id']}")

        return output[0]

    def provision(self, base_distro_name, ssh_key_path, is_update_vm=False, instance_id=None):
        """
        Provisions a new transient VM instance from an existing base image.

        The instance is created with a virtio UNIX channel so that @wait can
        block until the VM is online.

        :param base_distro_name: which distro template to use as string
        :param size: capacity of the underlying storage in GB, default is 50
        """

        log.debug(f"Provisioning machine '{self.name}'")

        provision_poolname = self.config["poolname"]
        if "volatile" in self.config and self.config["volatile"]:
            provision_poolname = self.config["volatile_poolname"]

        base_image = base_distro_name
        snapshots = SnapshotManager().get_snapshots(base_distro_name)
        if snapshots:
            base_image = snapshots[-1].name()

        # create the storage for the VM first
        libvirt_handle = LibvirtHandle()
        architecture = libvirt_handle.get_architecture(base_image)
        if architecture is None:
            raise Exception(f"Failed to determine the architecture of base VM '{base_image}'")

        cpu = self.config["cpu"]
        if architecture == "i686":
            cpu = self.config["cpu_32bit"]

        if self.config["force"]:
            libvirt_handle.cleanup_machine(self.name)
            libvirt_handle.cleanup_storage(self.name + ".qcow2",
                                                provision_poolname)

        volume = libvirt_handle.create_volume(self.name, self.config["disk_size"],
                                              base_image, self.config["poolname"],
                                              provision_poolname)

        user_data = cloud_init.get_user_data(self.name)
        if instance_id is None:
            meta_data = cloud_init.get_meta_data(self.name)
        else:
            meta_data = cloud_init.get_meta_data(instance_id)
        user_data_file = self._dump_user_data(user_data)
        meta_data_file = self._dump_meta_data(meta_data)
        cloud_init_iso = None
        cloud_init_flag = f"--cloud-init user-data={user_data_file},meta-data={meta_data_file}"

        if self.config["generate_cloud_init_iso"]:
            self._delete_cloud_init_iso()
            cloud_init_iso = f"/tmp/{self.name}-cloud-init.iso"
            run_command(["genisoimage",
                         "-J",
                         "-rational-rock",
                         "-graft-points",
                         "-input-charset", "utf-8",
                         "-V", "cidata",
                         "-output", cloud_init_iso,
                         f"user-data={user_data_file}",
                         f"meta-data={meta_data_file}",
                         ], debug=self.config["debug"])
            cloud_init_flag = f"--disk path={cloud_init_iso},device=cdrom"

        transient_flag = "--transient"
        if "persistent" in self.config and self.config["persistent"]:
            transient_flag = ""

            log.debug("VM is persistent, pulling changes to make the image standalone.")
            run_command([
                "qemu-img", "rebase", "-b", "", volume.path(),
                ], debug=self.config["debug"])

        hostdev_flag = ""
        if "vendor_id" in self.config and "device_id" in self.config and \
           self.config["vendor_id"] and self.config["device_id"]:
            pcie_address = self.find_pci_address()
            hostdev_flag = f"--hostdev {pcie_address},address.type=pci"

        if is_update_vm:
            transient_flag = ""

        cmd = shlex.split(self.config["virt_install_command"].format(
            name=self.name,
            poolname=provision_poolname,
            ram_size=self.config["ram_size"],
            vcpus=self.config["vcpus"],
            network=self.config["network"],
            transient_flag=transient_flag,
            cloud_init_flag=cloud_init_flag,
            architecture=architecture,
            hostdev_flag=hostdev_flag,
            cpu=cpu,
        ))

        if not self.config["debug"]:
            cmd.append("--quiet")

        if (self.config["ci_job_token"] is not None and
                self.config["gitlab"]["upload_console"]):
            cmd.extend(["--serial",
                        f"file,path='/tmp/{self.name}-serial-console.log'"])

        # virt-install may fail for various reasons, e.g. if too many
        # concurrent instances are trying to refresh its own created
        # 'boot-scratch' storage pool to store user cloud-init configs.
        # So, since we have no control over this, let's give virt-install a few
        # re-tries before failing fatally with the last active exception
        save_ex = None
        for retry in range(3):
            try:
                subprocess.run(cmd, capture_output=not self.config["debug"], check=True)
                break
            except subprocess.CalledProcessError as ex:
                log.debug(f"{ex}")
                log.debug(f"Re-trying command '{cmd}'")
                save_ex = ex
                self.teardown()
                continue
        else:
            log.debug("Provision re-try limit reached")
            self.teardown()
            raise save_ex

        log.debug("Removing known_hosts entry")
        run_command([
            "ssh-keygen", "-R", self.name
            ], debug=self.config["debug"], can_fail=True)

        try:
            libvirt_handle.set_target(self.name, libvirt_handle.get_target(base_image))
            ssh_wait(self.name, ssh_key_path, self.config["ssh_user"], self.config["ssh_timeout"])
        except Exception as e:
            log.debug(f"Error occured while setting target {e}")
            self.teardown()
        finally:
            os.unlink(user_data_file)
            os.unlink(meta_data_file)

    def teardown(self):
        """Cleans up the VM instance along with its block storage overlay."""

        log.debug(f"Cleaning up '{self.name}' resources")

        try:
            libvirt_handle = LibvirtHandle()
            libvirt_handle.cleanup_machine(self.name)
            self._delete_cloud_init_iso()
            libvirt_handle.cleanup_storage(self.name + ".qcow2", self.config["poolname"])
            libvirt_handle.cleanup_storage(self.name + ".qcow2", self.config["volatile_poolname"])
        finally:
            if (self.config["ci_job_token"] is not None and
                    self.config["gitlab"]["upload_console"]):
                log.debug("Uploading console log")
                cmd = [
                    "gitlab-runner", "artifacts-uploader",
                    "--expire-in", self.config["gitlab"]["artifact_expire"],
                    "--url", "https://gitlab.com",
                    "--token", self.config['ci_job_token'],
                    "--id", self.config['job_id'],
                    "--path", f"/tmp/{self.name}-serial-console.log",
                    "--artifact-type", "archive"
                ]
                run_command(cmd, cwd="/tmp",
                            debug=self.config["debug"])

                try:
                    os.unlink(f"/tmp/{self.name}-serial-console.log")
                except FileNotFoundError:
                    pass
            if self.config["generate_cloud_init_iso"]:
                try:
                    os.unlink(f"/tmp/{self.name}-cloud-init.iso")
                except FileNotFoundError:
                    pass
