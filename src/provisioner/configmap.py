# config.py - module containing the project's configmap definition
#
# Copyright (C) 2021 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import logging

from provisioner.singleton import Singleton
from provisioner.config import Config
from os.path import abspath, expanduser

log = logging.getLogger(__name__)


class ConfigMap(metaclass=Singleton):
    """
    Global configuration map instance.

    A dictionary-like class. An instance of this class manages configuration
    for an application run. This class is supposed to take care of any
    configuration source precedence handling.
    """

    def __init__(self, **kwargs):
        opts = [
            "action",
            "debug",
            "distro",
            "executable",
            "exec_args",
            "machine",
            "script",
            "ssh_key_file",
            "poolname",
            "disk_size",
            "volatile",
        ]

        self._values = dict(zip(opts, [None] * len(opts)))

        lcitool_config_path = kwargs.pop("lcitool_config", None)

        config = Config(path=kwargs.pop("config", None))
        self._values.update(config.values)
        self._values.update((k,v) for k,v in kwargs.items() if v is not None)

        if lcitool_config_path:
            self._values["lcitool"]["config"] = abspath(expanduser(lcitool_config_path))

    def __contains__(self, item):
        return item in self._values

    def __getitem__(self, key):
        if type(key) is not str:
            raise TypeError

        return self._values[key]

    def __setitem__(self, key, value):
        if type(key) is not str:
            raise TypeError

        self._values[key] = value
