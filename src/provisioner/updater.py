# libvirt_handle.py - module containing the libvirt wrapper
#
# Copyright (C) 2023 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import libvirt
import logging
import os

from pathlib import Path
from provisioner.configmap import ConfigMap
from provisioner.libvirt_handle import LibvirtHandle
from provisioner.machine import Machine
from provisioner.snapshot_manager import SnapshotManager
from provisioner.util import ssh_wait, check_commands_present, run_command


log = logging.getLogger(__name__)


class LcitoolUpdater:
    """Helper for making snapshots and invoking lcitool to update it"""

    def __init__(self, config=None):
        self.config = ConfigMap() if config is None else config
        self.snapshot_manager = SnapshotManager(self.config)
        self.libvirt_handle = LibvirtHandle()

        required_commands = [
            "ssh-keygen",
            "lcitool",
            "ansible-playbook",
            "virt-sysprep",
        ]
        if self.config["git_update_paths"]:
            required_commands.append("git")

        check_commands_present(required_commands)

    def revert_distro(self, distro):
        snapshots = self.snapshot_manager.get_snapshots(distro)

        if len(snapshots) < 2 and not self.config["force"]:
            raise Exception("Can't revert the last snapshot (use --force or reinstall the distro)")

        log.debug(f"Destroying latest distro snapshot '{snapshots[-1].name()}'")
        self.libvirt_handle.cleanup_machine(snapshots[-1].name())
        self.libvirt_handle.cleanup_storage(snapshots[-1].name() + ".qcow2",
                                            self.config["poolname"])

    def uninstall_distro(self, distro):
        snapshots = self.snapshot_manager.get_snapshots(distro)

        for snapshot in snapshots:
            log.debug(f"Destroying distro snapshot '{snapshot.name()}'")
            self.libvirt_handle.cleanup_machine(snapshot.name())
            self.libvirt_handle.cleanup_storage(snapshot.name() + ".qcow2",
                                                self.config["poolname"])
        log.info(f"Uninstalled all snapshots for {distro} successfully.")

    def install_distro(self, distro, vm_name):
        if self.config["force"]:
            self.uninstall_distro(vm_name)

        if len(self.snapshot_manager.get_snapshots(vm_name)) > 0:
            raise Exception(f"The distro {distro} is already installed (use --force to reinstall it)")

        vm_name = vm_name + "-0"
        run_command([
            "ssh-keygen", "-R", vm_name
            ], debug=self.config["debug"], can_fail=True)

        data_dir_flag = []
        config_flag = []
        debug_flag = []

        if self.config['lcitool']['data_dir']:
            data_dir_flag = ["-d", self.config['lcitool']['data_dir']]
        if self.config['lcitool']['config']:
            config_flag = ["-c",  self.config['lcitool']['config']]
        if self.config['debug']:
            debug_flag = ["--debug"]

        log.info("Installing using lcitool")
        run_command([
            "lcitool",
            *debug_flag,
            *data_dir_flag,
            *config_flag,
            "install", vm_name,
            "--target", distro,
            ], debug=self.config["debug"])
        log.info("lcitool install started, waiting for shutdown..")

        try:
            self.libvirt_handle.wait_for_shutdown(vm_name,
                                                  timeout=self.config["install_timeout"],
                                                  send_shutdown=False)
            self.libvirt_handle.start_machine(vm_name)
            ssh_wait(vm_name, None, "root", self.config["install_timeout"])
            self.update_vm(vm_name)
        except Exception:
            self.libvirt_handle.cleanup_machine(vm_name)
            self.libvirt_handle.cleanup_storage(vm_name + ".qcow2",
                                                self.config["poolname"])
            raise

        log.info(f"The base image {vm_name} (based on distro {distro}) installed successfully")
        log.info("Please run the update command before using.")

    def update_vm(self, vm_name):
        # start VM and wait SSH
        for update_path in self.config["git_update_paths"]:
            log.debug(f"Updating {update_path}")

            run_command([
                "git",
                "-C", update_path,
                "pull", "--rebase"
                ], debug=self.config["debug"], can_fail=True)

        log.info("Removing known_hosts entry")
        run_command([
            "ssh-keygen", "-R", "cryptsetup-rhel-9-update"
            ], debug=self.config["debug"], can_fail=True)

        log.info("Waiting for update VM to start")
        ssh_wait(vm_name, self.config["ssh_key_file"], self.config["ssh_user"], self.config["ssh_timeout"])

        data_dir_flag = []
        config_flag = []

        if self.config['lcitool']['data_dir']:
            data_dir_flag = ["-d", self.config['lcitool']['data_dir']]
        if self.config['lcitool']['config']:
            config_flag = ["-c",  self.config['lcitool']['config']]

        log.info("Updating using lcitool")
        run_command([
            "lcitool",
            *data_dir_flag,
            *config_flag,
            "update", vm_name, "cryptsetup",
            ], debug=self.config["debug"])

        if self.config['lcitool']['data_dir']:
            log.info("Running custom post_update playbook")
            playbook_dir = os.path.join(self.config['lcitool']['data_dir'], "ansible", "post")
            run_command([
                "ansible-playbook", f"{playbook_dir}/main.yml",
                "-i", f"{playbook_dir}/inventory/",
                "--limit", vm_name,
                ], debug=self.config["debug"])

        log.info("Shutting down the machine...")
        self.libvirt_handle.shutdown_machine(vm_name,
                                             timeout=self.config["shutdown_timeout"])

        log.info("Cleaning the base image")
        run_command([
            "virt-sysprep",
            "--operations", "defaults,-ssh-userdir",
            "--truncate", "/etc/machine-id",
            "-d", vm_name,
            ], debug=self.config["debug"])

        log.info("Removing known_hosts entry")
        run_command([
            "ssh-keygen", "-R", "cryptsetup-rhel-9-update"
            ], debug=self.config["debug"], can_fail=True)

        log.info("Update finished successfully.")

    def run_acceptance_check(self, vm_name):
        run_command([
            "ssh-keygen", "-R", vm_name
            ], debug=self.config["debug"], can_fail=True)

        if self.config["ssh_key_file"] is None:
            raise Exception("No SSH key available")

        log.info(f"Waiting for acceptance test VM {vm_name} to start")
        ssh_wait(vm_name, self.config["ssh_key_file"], self.config["ssh_user"], self.config["ssh_timeout"])

        machine = Machine(vm_name)
        try:
            machine.connect(self.config["ssh_key_file"], self.config["ssh_user"])

            with open(self.config["acceptance_test_script"], "r"):
                # NADA - check that the file exists and we can read it
                pass

            basename = Path(self.config["acceptance_test_script"]).name
            dest = f"/tmp/{basename}"
            machine.conn.upload(self.config["acceptance_test_script"], dest)

            command_string = f"/bin/bash {dest}"
            if not self.config["debug"]:
                command_string += " >/dev/null 2>&1"

            log.info("Running acceptance check")
            return machine.conn.exec(command_string)
        except Exception as ex:
            raise Exception(
                f"Failed to execute acceptance test on '{vm_name}': {ex}")

    def update_is_in_progress(self, distro):
        update_snapshot_name = f"{distro}-update"
        update_test_snapshot_name = f"{distro}-update-test"
        storage_pool = self.libvirt_handle.conn.storagePoolLookupByName(self.config["poolname"])

        log.debug(f"Checking if update is in progress (looking for '{update_snapshot_name}' and '{update_test_snapshot_name}'")

        try:
            self.libvirt_handle.conn.lookupByName(update_snapshot_name)
            return True
        except libvirt.libvirtError as ex:
            if ex.get_error_code() != libvirt.VIR_ERR_NO_DOMAIN:
                raise

        try:
            self.libvirt_handle.conn.lookupByName(update_test_snapshot_name)
            return True
        except libvirt.libvirtError as ex:
            if ex.get_error_code() != libvirt.VIR_ERR_NO_DOMAIN:
                raise

        try:
            storage_pool.storageVolLookupByName(update_snapshot_name + ".qcow2")
            return True
        except libvirt.libvirtError as ex:
            if ex.get_error_code() != libvirt.VIR_ERR_NO_STORAGE_VOL :
                raise

        try:
            storage_pool.storageVolLookupByName(update_test_snapshot_name + ".qcow2")
            return True
        except libvirt.libvirtError as ex:
            if ex.get_error_code() != libvirt.VIR_ERR_NO_STORAGE_VOL :
                raise

        return False

    def cleanup_update_machines(self, distro):
        update_snapshot_name = f"{distro}-update"
        update_test_snapshot_name = f"{distro}-update-test"

        self.libvirt_handle.cleanup_machine(update_snapshot_name)
        self.libvirt_handle.cleanup_storage(update_snapshot_name + ".qcow2",
                                            self.config["poolname"])
        self.libvirt_handle.cleanup_machine(update_test_snapshot_name)
        self.libvirt_handle.cleanup_storage(update_test_snapshot_name + ".qcow2",
                                            self.config["poolname"])

    def update(self, distro):
        snapshots_limit = max(self.config["snapshots_limit"], 1)
        snapshots = self.snapshot_manager.get_snapshots(distro)

        if not snapshots:
            raise Exception(f"There is no update snapshot for the distro {distro}")

        if snapshots_limit > 1:
            self.snapshot_manager.delete_snapshots(distro, snapshots_limit)

        latest_snapshot = snapshots[-1]
        latest_snapshot_id = int(latest_snapshot.name().split("-")[-1])
        new_latest_snapshot = f"{distro}-{latest_snapshot_id + 1}"
        update_snapshot_name = f"{distro}-update"
        update_test_snapshot_name = f"{distro}-update-test"

        ssh_key_path = self.config["ssh_key_file"]
        if ssh_key_path is None:
            raise Exception("No SSH key available")

        if self.config["force"]:
            self.cleanup_update_machines(distro)

        if self.update_is_in_progress(distro):
            raise Exception("Update is already in progress (use --force to destroy update VM and restart update process).")

        log.info(f"Creating update VM {update_snapshot_name}")

        # create update VM
        machine = Machine(update_snapshot_name)
        try:
            # we need to set instance ID to something unique to trigger
            # cloud-init instance initialization
            machine.provision(latest_snapshot.name(),
                              ssh_key_path,
                              is_update_vm=True,
                              instance_id=f"{update_snapshot_name}-{latest_snapshot_id + 1}")
        except Exception as ex:
            self.cleanup_update_machines(distro)
            raise Exception(f"Failed to create the update VM '{update_snapshot_name}': {ex}")

        # update the VM
        try:
            self.update_vm(update_snapshot_name)
        except Exception as ex:
            self.cleanup_update_machines(distro)
            raise Exception(f"Failed to update the VM: {ex}")

        if self.config["acceptance_test_script"] is not None:
            log.info(f"Creating test VM {update_test_snapshot_name} from {update_snapshot_name} to run acceptance check")

            machine = Machine(update_test_snapshot_name)
            try:
                machine.provision(update_snapshot_name,
                                  ssh_key_path,
                                  is_update_vm=True,
                                  instance_id=f"{update_test_snapshot_name}-{latest_snapshot_id + 1}")
            except Exception as ex:
                self.cleanup_update_machines(distro)
                raise Exception(f"Failed to prepare acceptance test VM '{update_test_snapshot_name}': {ex}")

            try:
                retval = self.run_acceptance_check(update_test_snapshot_name)
                if retval != 0:
                    raise Exception(f"test script returned error code {retval}")
                log.info("Acceptance test PASSED")
            except Exception as ex:
                raise Exception(f"Acceptance check failed: {ex}")
            finally:
                self.libvirt_handle.cleanup_machine(update_test_snapshot_name)
                self.libvirt_handle.cleanup_storage(update_test_snapshot_name + ".qcow2",
                                                    self.config["poolname"])
        else:
            log.warning("Skipping acceptance check, because no script was specified (acceptance_test_script in config)")

        log.info(f"Moving update snapshot {update_snapshot_name} to {new_latest_snapshot}")
        self.snapshot_manager.rename_vm(update_snapshot_name, new_latest_snapshot)

        self.snapshot_manager.delete_snapshots(distro, snapshots_limit)
