# cmdline.py - module containing the command line parser
#
# Copyright (C) 2021 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import argparse


class BooleanOptionalAction(argparse.Action):
    def __init__(self,
                 option_strings,
                 dest,
                 default=None,
                 type=None,
                 choices=None,
                 required=False,
                 help=None,
                 metavar=None):

        _option_strings = []
        for option_string in option_strings:
            _option_strings.append(option_string)

            if option_string.startswith('--'):
                option_string = '--no-' + option_string[2:]
                _option_strings.append(option_string)

        super().__init__(
            option_strings=_option_strings,
            dest=dest,
            nargs=0,
            default=default,
            type=type,
            choices=choices,
            required=required,
            help=help,
            metavar=metavar)

    def __call__(self, parser, namespace, values, option_string=None):
        if option_string in self.option_strings:
            setattr(namespace, self.dest, not option_string.startswith('--no-'))

    def format_usage(self):
        return ' | '.join(self.option_strings)


class CmdLine:
    """
    Command line parser.
    """

    def __init__(self):
        self._parsers = {}

        self._parsers["__main__"] = argparse.ArgumentParser(
            conflict_handler="resolve",
            description="GitLab CI libvirt custom executor wrapper tool",
        )

        self._parsers["__main__"].add_argument(
            "--debug",
            help="display debugging information",
            action="store_true",
        )

        subparsers = self._parsers["__main__"].add_subparsers(metavar="COMMAND",
                                                              dest="action")
        subparsers.required = True

        sshkeyopt = argparse.ArgumentParser(add_help=False)
        sshkeyopt.add_argument(
            "--ssh-key-file",
            metavar="PATH",
            help="path to SSH private key"
        )

        for command in ["prepare", "run", "cleanup", "update", "install",
                        "uninstall", "revert"]:
            parser = subparsers.add_parser(
                command,
                help=f"GitLab {command} stage parser",
                parents=[sshkeyopt]
            )

            parser.add_argument(
                "-c", "--config",
                help="path to the config file",
            )

            self._parsers[command] = parser

        for command in ["prepare", "run", "cleanup", "install"]:
            self._parsers[command].add_argument(
                "-m", "--machine",
                help="machine instance to operate on",
            )

        for command in ["prepare", "update", "install", "uninstall", "revert"]:
            self._parsers[command].add_argument(
                "-d", "--distro",
                help="what OS distro base image to use",
            )
        for command in ["update", "install", "revert", "prepare"]:
            self._parsers[command].add_argument(
                "-f", "--force",
                help="force the operation (removing previous installation/update in progress)",
                action="store_true",
            )

        for command in ["update", "install"]:
            self._parsers[command].add_argument(
                "--lcitool-config",
                help="path to config file for lcitool",
            )

        try:
            opt_boolean = argparse.BooleanOptionalAction
        except AttributeError:
            opt_boolean = BooleanOptionalAction

        for command in ["prepare", "cleanup"]:
            self._parsers[command].add_argument(
                "--volatile",
                help="Allocate volatile disk snapshot for the VM (VM disk will be lost on host shutdown)",
                action=opt_boolean,
            )

        self._parsers["prepare"].add_argument(
            "-p", "--poolname",
            help="libvirt pool name for the base images and transient VM volumes",
        )
        self._parsers["prepare"].add_argument(
            "-s", "--disk-size",
            help="size of the transient VM disk volume (in GB)",
        )
        self._parsers["prepare"].add_argument(
            "-r", "--ram-size",
            help="amount of RAM allocated to the VM (in MB)",
        )
        self._parsers["prepare"].add_argument(
            "-v", "--vcpus",
            help="number of cores allocated to the VM",
        )
        self._parsers["prepare"].add_argument(
            "-n", "--network",
            help="libvirt network for the VM",
        )
        self._parsers["prepare"].add_argument(
            "--persistent",
            help="libvirt network for the VM",
            action="store_true",
        )
        self._parsers["prepare"].add_argument(
            "--vendor-id",
            help="vendor ID of the PCIe device to pass through to the VM",
        )
        self._parsers["prepare"].add_argument(
            "--device-id",
            help="device ID of the PCIe device to pass through to the VM",
        )

        self._parsers["run"].add_argument(
            "executable",
            nargs="?",
            help="Absolute path to the executable",
        )
        self._parsers["run"].add_argument(
            "exec_args",
            nargs="*",
            help="Arguments to be passed to the executable",
        )
        self._parsers["run"].add_argument(
            "--script",
            help="Upload and execute a script instead of a command",
            action="store_true",
        )

        self._parsers["cleanup"].add_argument(
            "-p", "--poolname",
            help="libvirt pool name for the base images and transient VM volumes",
        )

    def parse(self):
        """Parses the command line arguments (Argparse entry point)."""

        return self._parsers["__main__"].parse_args()
