# application.py - module containing the libvirt-gci application code
#
# Copyright (C) 2021 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import os

from pathlib import Path

from provisioner.configmap import ConfigMap
from provisioner.machine import Machine
from provisioner.singleton import Singleton
from provisioner.updater import LcitoolUpdater
from time import sleep

log = logging.getLogger(__name__)


class Application(metaclass=Singleton):
    def __init__(self, cli_args):
        # initialize the global configuration map singleton object
        configmap = ConfigMap(**cli_args)

        # GitLab will set these with every job
        project = os.environ.get("CUSTOM_ENV_CI_PROJECT_NAME")
        job_id = os.environ.get("CUSTOM_ENV_CI_JOB_ID")
        ci_job_token = os.environ.get("CUSTOM_ENV_CI_JOB_TOKEN")
        distro = os.environ.get("CUSTOM_ENV_DISTRO")
        disk_size = os.environ.get("CUSTOM_ENV_DISK_SIZE")

        vendor_id = os.environ.get("CUSTOM_ENV_PCI_PASSTHROUGH_VENDOR_ID")
        device_id = os.environ.get("CUSTOM_ENV_PCI_PASSTHROUGH_DEVICE_ID")
        volatile = os.environ.get("CUSTOM_ENV_VOLATILE")
        if volatile:
            volatile = bool(int(volatile))

        if distro is not None:
            configmap["distro"] = distro
        if disk_size is not None:
            configmap["disk_size"] = disk_size
        if vendor_id is not None:
            configmap["vendor_id"] = vendor_id
        if device_id is not None:
            configmap["device_id"] = device_id
        if volatile is not None:
            configmap["volatile"] = volatile

        configmap["project"] = project
        configmap["job_id"] = job_id
        configmap["ci_job_token"] = ci_job_token
        configmap["ssh_key_file"] = self._get_ssh_key_path(configmap)

    @staticmethod
    def _get_ssh_key_path(configmap):
        if configmap["ssh_key_file"] is None:
            for identity in ["id_ed25519", "id_rsa"]:
                p = Path(Path.home(), ".ssh", identity)
                if p.exists():
                    configmap["ssh_key_file"] = p
                    break

        return configmap["ssh_key_file"]

    def _get_machine_name(self):
        configmap = ConfigMap()
        name = configmap["machine"]
        project = configmap["project"]
        distro = configmap["distro"]
        job_id = configmap["job_id"]

        # GitLab-driven provision
        if all([project, distro, job_id]):
            name = f"gitlab-{project}-{distro}-{job_id}"
        else:
            # Manual provision
            if configmap["action"] == "prepare":
                if distro is None:
                    raise Exception("No distro specified for manual execution")

                if name is None:
                    import random
                    from string import ascii_letters

                    randstr = "".join(random.sample(ascii_letters, 8))
                    name = f"{distro}-{randstr}"

            elif name is None:
                raise Exception("No machine name specified for manual execution")

        log.debug(f"Machine will be called '{name}'")
        return name

    def _action_prepare(self):
        """Provisions a new VM using libvirt."""

        configmap = ConfigMap()

        machine_name = self._get_machine_name()
        machine = Machine(machine_name)

        try:
            ssh_key_path = self._get_ssh_key_path(configmap)
            if ssh_key_path is None:
                raise Exception("No SSH key available")

            machine.provision(configmap["distro"], ssh_key_path)
        except Exception as ex:
            raise Exception(f"Failed to provision machine '{machine_name}': {ex}")

        try:
            psid = None
            for psid_spec in configmap["psid"]:
                if "vendor_id" not in psid_spec or "device_id" not in psid_spec:
                    continue
                if "vendor_id" not in configmap or "device_id" not in configmap:
                    continue
                if psid_spec["vendor_id"] != configmap['vendor_id'] or \
                        psid_spec["device_id"] != configmap['device_id']:
                    continue

                psid = psid_spec["psid"]
                break

            if psid:
                for retry in range(5):
                    try:
                        ssh_key_path = self._get_ssh_key_path(configmap)
                        if ssh_key_path is None:
                            raise Exception("No SSH key available")

                        machine.connect(ssh_key_path, configmap["ssh_user"])
                        machine.conn.upload_string_as_file(psid, configmap["psid_upload_location"])

                        return 0
                    except Exception as ex:
                        log.debug(f"{ex}")
                        log.debug("Re-trying upload PSID")

                        save_ex = Exception(
                                f"Failed to upload PSID file on '{machine_name}': {ex}")
                        sleep((retry + 1) * 2)
                else:
                    log.debug("Run re-try limit reached")
                    raise save_ex
        except Exception as ex:
            machine.teardown()
            raise Exception(f"Failed to prepare machine '{machine_name}': {ex}")

        return 0

    def _action_run(self):
        """Executes a command/script remotely on the given VM."""

        configmap = ConfigMap()

        machine_name = self._get_machine_name()
        cmd_str = configmap["executable"]
        cmd_args = configmap["exec_args"]
        machine = Machine(machine_name)

        save_ex = None
        for retry in range(5):
            try:
                ssh_key_path = self._get_ssh_key_path(configmap)
                if ssh_key_path is None:
                    raise Exception("No SSH key available")

                machine.connect(ssh_key_path, configmap["ssh_user"])

                if configmap["script"]:
                    basename = Path(configmap["executable"]).name

                    with open(configmap["executable"], "r"):
                        # NADA - check that the file exists and we can read it
                        pass

                    dest = f"/tmp/{basename}"
                    machine.conn.upload(configmap["executable"], dest)
                    cmd_str = "/bin/bash"
                    cmd_args = [dest] + configmap["exec_args"]

                cmdlinestr = f"{cmd_str} {' '.join(cmd_args)}"
                return machine.conn.exec(cmdlinestr)
            except Exception as ex:
                log.debug(f"{ex}")
                log.debug("Re-trying running workload")

                save_ex = Exception(
                    f"Failed to execute workload on '{machine_name}': {ex}")
                sleep((retry + 1) * 2)
        else:
            log.debug("Run re-try limit reached")
            raise save_ex

    def _action_cleanup(self):
        """Cleans up the VM (including storage) given a name."""
        machine_name = self._get_machine_name()
        try:
            Machine(machine_name).teardown()

            return 0
        except Exception as ex:
            raise Exception(f"Failed to clean-up machine {machine_name}: {ex}")

    def _action_update(self):
        """Updates base VM using lcitool"""
        configmap = ConfigMap()
        configmap["volatile"] = False

        updater = LcitoolUpdater(configmap)
        updater.update(configmap["distro"])

        return 0

    def _action_revert(self):
        """Delets latest snaphot and moves to the previous one"""
        configmap = ConfigMap()
        configmap["volatile"] = False

        distro = configmap["distro"]

        if not distro:
            raise Exception("You must specify the distro name.")

        updater = LcitoolUpdater(configmap)
        updater.revert_distro(distro)

        return 0

    def _action_uninstall(self):
        """Installs base VM using lcitool"""
        configmap = ConfigMap()
        configmap["volatile"] = False

        distro = configmap["distro"]

        if not distro:
            raise Exception("You must specify the distro name.")

        updater = LcitoolUpdater(configmap)
        updater.uninstall_distro(distro)

        return 0

    def _action_install(self):
        """Installs base VM using lcitool"""
        configmap = ConfigMap()
        configmap["volatile"] = False

        machine_name = configmap["machine"]
        distro = configmap["distro"]

        if not all([machine_name, distro]):
            raise Exception("You must specify both, machine name and distro name.")

        updater = LcitoolUpdater(configmap)
        updater.install_distro(distro, machine_name)

        return 0

    def run(self):
        """
        Application entry point.

        Selects an action callback according to the CLI subcommand.
        """

        cb = self.__getattribute__("_action_" + ConfigMap()["action"])
        return cb()
