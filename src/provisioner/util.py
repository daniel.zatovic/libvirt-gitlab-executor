# util.py - module hosting utility functions for lcitool
#
# Copyright (C) 2017-2020 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import errno
import logging
import os
import shutil
import subprocess
import sys

from pathlib import Path
from provisioner.ssh import SSHConn
from time import sleep

log = logging.getLogger(__name__)


class CommandNotFound(Exception):
    """Exception raised when command is not found

    Attributes:
        command -- name of the missing command
    """

    def __init__(self, commands):
        self.commands = commands
        super().__init__(f"Required command{'s' if len(commands) > 1 else ''} {', '.join(commands)} not found.")


def check_commands_present(commands):
    missing_commands = []
    for command in commands:
        if shutil.which(command) is None:
            missing_commands.append(command)
    if missing_commands:
        raise CommandNotFound(missing_commands)


def run_command(cmd, debug=False, attempts=3, can_fail=False, cwd=None, interval=1):
    log.debug(f"Running command '{' '.join(cmd)}'")

    save_ex = None
    for retry in range(attempts):
        sleep(interval)
        try:
            subprocess.run(cmd, capture_output=not debug, check=not can_fail, cwd=cwd)
            break
        except subprocess.CalledProcessError as ex:
            log.debug(f"{ex}")
            log.debug(f"Re-trying command '{cmd}'")
            save_ex = ex
            continue
    else:
        log.debug("Command re-try limit reached")
        raise save_ex


def get_command_output(cmd):
    process = subprocess.run(cmd,
                             check=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
    return process.stdout.decode("utf-8")


def get_config_dir():
    try:
        config_dir = Path(os.environ["XDG_CONFIG_HOME"])
    except KeyError:
        config_dir = Path.home().joinpath(".config")

    return Path(config_dir, "libvirt-gci")


def package_resource(package, relpath):
    """
    Backcompatibility helper to retrieve a package resource using importlib

    :param package: object conforming to importlib.resources.Package requirement
    :param relpath: relative path to the actual resource (or directory) as
                    str or Path object
    :returns: a Path object to the resource
    """

    from importlib import import_module, resources

    if hasattr(resources, "files"):
        return Path(resources.files(package), relpath)
    else:
        # This is a horrible hack, it won't work for resources that don't exist
        # on the file system (which should not be a problem for our use case),
        # but it's needed because importlib.resources.path only accepts
        # filenames for a 'Resource' [1]. What it means is that one cannot pass
        # a path construct in 'relpath', because 'Resource' cannot contain
        # path delimiters and also cannot be a directory, so we cannot use the
        # method to construct base resource paths.
        # [1] https://docs.python.org/3/library/importlib.resources.html?highlight=importlib%20resources#importlib.resources.path
        # Instead, we'll extract the package path from ModuleSpec (loading the
        # package first if needed) and then concatenate it with the 'relpath'
        #
        # TODO: Drop this helper once we move onto 3.9+
        if package not in sys.modules:
            import_module(package)

        package_path = Path(sys.modules[package].__file__).parent
        return Path(package_path, relpath)


def ssh_wait(hostname, ssh_key_path, ssh_user, ssh_timeout):
    from paramiko import ssh_exception
    from socket import EAI_NONAME

    logging.getLogger("paramiko").setLevel(logging.WARNING)

    # we need to give the machine a head start (up to timeout seconds)
    # to get an IP lease first and only then we can try SSHing into the
    # machine (wait in 2s increments)
    timeout = ssh_timeout
    seconds = 2
    for _ in range(timeout // seconds):
        sleep(seconds)
        try:
            # if ssh_key_path is None, we only use default root password
            conn = SSHConn(hostname)
            try:
                # first-time install via lcitool has the following default login
                conn.connect_password(password="root", username="root")
                return
            except Exception as ex:
                if ssh_key_path is not None:
                    conn.connect(key_filepath=ssh_key_path,
                                 username=ssh_user)
                    return
                raise ex
        except ssh_exception.NoValidConnectionsError as ex:
            # NoValidConnectionsError is a subclass of various socket
            # errors which in turn is a subclass of OSError. We're
            # specifically interested in EHOSTUNREACH and ECONNREFUSED
            # errnos which we can ignore for the duration of the timeout
            # period
            for error in ex.errors.values():
                if isinstance(error, OSError):
                    if error.errno == errno.EHOSTUNREACH or error.errno == errno.ECONNREFUSED:
                        break
            else:
                raise ex
        except ssh_exception.SSHException:
            continue
        except OSError as ex:
            if ex.errno == EAI_NONAME:  # Name or service not known
                continue
            raise ex

    raise Exception(f"Failed to connect to {hostname}: timeout reached")
