# config.py - module containing configuration file handling primitives
#
# Copyright (C) 2017-2023 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import copy
import logging
import yaml

from pathlib import Path
from provisioner import util

log = logging.getLogger(__name__)


class ConfigError(Exception):
    """
    Global exception type for the config module.

    Contains a detailed message coming from one of its subclassed exception
    types.
    """

    def __init__(self, message):
        super().__init__(message, "Configuration")


class LoadError(ConfigError):
    """Thrown when the configuration for lcitool could not be loaded."""

    def __init__(self, message):
        message_prefix = "Failed to load config: "
        message = message_prefix + message
        super().__init__(message)


class ValidationError(ConfigError):
    """Thrown when the configuration for lcitool could not be validated."""

    def __init__(self, message):
        message_prefix = "Failed to validate config: "
        message = message_prefix + message
        super().__init__(message)


class Config:

    @property
    def values(self):

        # lazy evaluation: most lcitool actions actually don't need the config
        if self._values is None:
            values = self._load_config()
            self._validate(values)
            self._values = values
        return self._values

    def __init__(self, path=None):
        self._values = None
        self._config_file_paths = None

        if path is not None:
            path = Path(path)
            if not path.exists():
                raise LoadError(f"The specified config file {path} doesn't exist.")
            self._config_file_paths = [path]
        else:
            self._config_file_paths = [
                Path(util.get_config_dir(), fname) for fname in ["config.yml",
                                                                 "config.yaml"]
            ]

    def _load_config(self):
        # Load the template config containing the defaults first, this must
        # always succeed.
        default_config_path = util.package_resource(__package__,
                                                    "etc/config.yml")
        with open(default_config_path, "r") as fp:
            default_config = yaml.safe_load(fp)

        user_config_path = None
        for user_config_path in self._config_file_paths:
            #user_config_path = Path(user_config_path.name)
            if not user_config_path.exists():
                log.debug(f"{user_config_path.as_posix()} does not exist {user_config_path.name}")
                continue

            user_config_path_str = user_config_path.as_posix()
            log.debug(f"Loading configuration from '{user_config_path_str}'")
            try:
                with open(user_config_path, "r") as fp:
                    user_config = yaml.safe_load(fp)
                    if user_config is None:
                        user_config = {}
            except Exception as e:
                raise LoadError(f"'{user_config_path.name}': {e}")

            break
        else:
            user_config = {}

        # delete user params we don't recognize
        self._sanitize_values(user_config, default_config)

        # Override the default settings with user config
        values = self._merge_config(default_config, user_config)

        return values

    @staticmethod
    def _remove_unknown_keys(_dict, known_keys):
        keys = list(_dict.keys())

        for k in keys:
            if k not in known_keys:
                log.debug(f"Removing unknown key '{k}' from config")

                del _dict[k]

    @staticmethod
    def _remove_different_types(_dict, template_dict):
        keys = list(_dict.keys())
        for key in keys:
            if template_dict[key] is None:
                continue
            if type(_dict[key]) != type(template_dict[key]):
                log.debug(f"Removing key '{key}' from config due to incorrect type"
                    f" (should be {type(template_dict[key]).__name__} but is {type(_dict[key]).__name__})")
                del _dict[key]

    @staticmethod
    def _merge_config(default_config, user_config):
        config = copy.deepcopy(default_config)
        for section in default_config.keys():
            if section in user_config:
                log.debug(f"Applying user values: '{user_config[section]}'")

                if isinstance(user_config[section], dict):
                    config[section].update(user_config[section])
                else:
                    config[section] = user_config[section]
        return config

    def _sanitize_values(self, user_config, default_config):
        # remove keys we don't recognize
        self._remove_unknown_keys(user_config, default_config.keys())
        self._remove_different_types(user_config, default_config)
        for section in default_config.keys():
            if section in user_config and isinstance(default_config[section], dict):
                self._remove_unknown_keys(user_config[section],
                                          default_config[section].keys())
                self._remove_different_types(user_config[section], default_config[section])

    def _validate_keys(self, values, pathprefix=""):
        log.debug(f"Validating section='[{pathprefix}]'")

        # check that all keys have values assigned and of the right type
        for key, value in values.items():
            if isinstance(value, dict):
                self._validate_keys(value, pathprefix + "." + key)
                continue

            # if value is None:
            #     raise ValidationError(f"Missing value for '{pathprefix}.{key}'")

            if not isinstance(value, (str, int, list, type(None))):
                raise ValidationError(f"Invalid type for key '{pathprefix}.{key}'")

    def _validate(self, values):
        self._validate_keys(values)

        if not isinstance(values["ssh_key_file"], (str, type(None))):
            raise ValidationError("Invalid type for ssh_key_file (must be str or null)")
