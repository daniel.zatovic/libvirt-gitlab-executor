# libvirt_handle.py - module containing the libvirt wrapper
#
# Copyright (C) 2023 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import logging
import math
import os
import shutil

import xml.etree.ElementTree as ET

from provisioner.configmap import ConfigMap
from provisioner.libvirt_handle import LibvirtHandle
from provisioner.util import check_commands_present, run_command


log = logging.getLogger(__name__)


class SnapshotManager:
    """Helper for managing VM snapshots"""

    def __init__(self, config=None):
        self.config = ConfigMap() if config is None else config
        self.libvirt_handle = LibvirtHandle()

        check_commands_present(["qemu-img"])

    def get_snapshots(self, distro):
        import re
        p = re.compile(f'^{distro}-\\d+$')

        snapshots = [domain for domain in self.libvirt_handle.conn.listAllDomains() if p.match(domain.name())]
        snapshots.sort(key=lambda x: int(x.name().split("-")[-1]))
        return snapshots

    def delete_oldest_snapshot(self, distro):
        snapshots = self.get_snapshots(distro)
        if len(snapshots) <= 1:
            if len(snapshots) == 1:
                self.libvirt_handle.cleanup_machine(snapshots[0].name())
                self.libvirt_handle.cleanup_storage(snapshots[0].name() + ".qcow2",
                                                    self.config["poolname"])
            return

        self.libvirt_handle.shutdown_machine(snapshots[0].name(),
                                             timeout=self.config["shutdown_timeout"])
        self.libvirt_handle.shutdown_machine(snapshots[1].name(),
                                             timeout=self.config["shutdown_timeout"])

        pool = self.libvirt_handle.conn.storagePoolLookupByName(self.config["poolname"])
        vol = pool.storageVolLookupByName(snapshots[1].name() + ".qcow2")
        base_vol = pool.storageVolLookupByName(snapshots[0].name() + ".qcow2")

        assert self.config["delete_interval"] >= 1

        log.debug(f"Commiting {vol.path()} to base image.")
        run_command([
            'qemu-img',
            'commit',
            vol.path()
            ],
            debug=self.config["debug"],
            interval=self.config["delete_interval"],
            attempts=math.ceil(self.config["delete_timeout"] /
                               self.config["delete_interval"]))
        shutil.move(base_vol.path(), vol.path())

        self.libvirt_handle.cleanup_machine(snapshots[0].name())
        self.libvirt_handle.cleanup_storage(snapshots[0].name() + ".qcow2",
                                            self.config["poolname"])

    def delete_snapshots(self, distro, limit):
        snapshots = self.get_snapshots(distro)
        for _ in range(len(snapshots) - limit):
            self.delete_oldest_snapshot(distro)

    def clone_vm_xml(self, original_name, new_name):
        from uuid import uuid4
        original = self.libvirt_handle.conn.lookupByName(original_name)
        xml = ET.fromstring(original.XMLDesc())

        xml.find("name").text = new_name
        xml.find("uuid").text = str(uuid4())

        devices = xml.find("devices")
        for disk in devices.findall("disk"):
            source = disk.find("source")
            if source is None:
                continue

            if "file" in source.attrib and original_name not in source.attrib["file"]:
                continue
            if "volume" in source.attrib and original_name not in source.attrib["volume"]:
                continue

            disk.attrib["type"] = "volume"
            backing_store = disk.find("backingStore")
            if backing_store is not None:
                disk.remove(backing_store)

            source.attrib.pop("file", None)
            source.attrib["volume"] = new_name + ".qcow2"
            source.attrib["pool"] = self.config["poolname"]

        return ET.tostring(xml, encoding="unicode")

    def rename_volume(self, vol, new_name):
        volxml = ET.fromstring(vol.XMLDesc())
        volxml.find("name").text = new_name

        for root_name in ["target", "backingStore"]:
            root = volxml.find(root_name)
            if root:
                for child_name in ["permissions", "timestamps"]:
                    child = root.find(child_name)
                    if child:
                        root.remove(child)

        target = volxml.find("target")
        path = target.find("path")
        key = volxml.find("key")

        for element in [path, key]:
            dirname = os.path.dirname(element.text)
            element.text = os.path.join(dirname, new_name)

        pool = self.libvirt_handle.conn.storagePoolLookupByName(self.config["poolname"])
        newvol = pool.createXML(ET.tostring(volxml, encoding="unicode"))

        os.replace(vol.path(), path.text)
        vol.delete()

        return newvol

    def rename_vm(self, origin, new_name):
        self.libvirt_handle.shutdown_machine(origin,
                                             timeout=self.config["shutdown_timeout"])

        # TODO move to function
        pool = self.libvirt_handle.conn.storagePoolLookupByName(self.config["poolname"])
        vol = pool.storageVolLookupByName(origin + ".qcow2")
        self.rename_volume(vol, new_name + ".qcow2")

        xml = self.clone_vm_xml(origin, new_name)
        new_domain = self.libvirt_handle.conn.defineXML(xml)

        self.libvirt_handle.cleanup_machine(origin)

        return new_domain

    def make_snapshot_vm(self, origin, new_name):
        self.libvirt_handle.create_volume(new_name + ".qcow2",
                                          30, origin,
                                          poolname=self.config["poolname"])

        try:
            xml = self.clone_vm_xml(origin, new_name)
            self.libvirt_handle.conn.defineXML(xml)
        except Exception:
            self.libvirt_handle.cleanup_storage(new_name + ".qcow2",
                                                poolname=self.config["poolname"])
            raise
