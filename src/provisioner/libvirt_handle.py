# libvirt_handle.py - module containing the libvirt wrapper
#
# Copyright (C) 2021 Red Hat, Inc.
#
# SPDX-License-Identifier: GPL-2.0-or-later

import libvirt
import logging
import textwrap
import xml.etree.ElementTree as xmlparser


log = logging.getLogger(__name__)

LCITOOL_XMLNS = "http://libvirt.org/schemas/lcitool/1.0"


class LibvirtHandle:
    """Convenience wrapper for the libvirt library."""

    __instance = None

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = super(LibvirtHandle, cls).__new__(cls)
        return cls.__instance

    def __init__(self, uri="qemu:///system"):
        def nop_error_handler(_T, iterable):
            return None

        # Disable libvirt's default console error logging
        libvirt.registerErrorHandler(nop_error_handler, None)
        self.conn = libvirt.open(uri)

    def __del__(self):
        self.conn.close()

    def _get_base_image(self, name, poolname):
        """
        Looks up the template base image.

        :param name: Name of the base image template as string
        :param poolname: Name of the storage pool to search as string
        """

        log.debug(f"Looking up base image: name={name},poolname={poolname}")

        pool = self.conn.storagePoolLookupByName(poolname)
        vol = None
        for vol in pool.listAllVolumes():
            if name == vol.name():
                return vol
        else:
            raise Exception(f"Base image '{name}' not found")

    def create_volume(self, volname, size, distro, poolname="default", provision_poolname=None):
        """
        Creates an overlay volume for the given machine.

        :param volname: name of the volume to be created as string
        :param size: capacity of the volume as string/int
        :param distro: which distro template image to look for as string
        :param poolname: which libvirt storage pool to search for the template
                         image as string
        :param provision_poolname: in which libvirt storage pool should the
                         overlay image be created
        """

        log.debug(f"Creating overlay volume: poolname={poolname},"
                  f"distro={distro},volname={volname},size={size}")

        template = """
        <volume>
          <name>{name}</name>
          <capacity unit='G'>{size}</capacity>
          <target>
            <format type='qcow2'/>
          </target>
          <backingStore>
            <path>{backing_vol_path}</path>
            <format type='{backing_vol_format}'/>
          </backingStore>
        </volume>
        """

        if provision_poolname is None:
            provision_poolname = poolname

        # get the base image for the volume
        base_img_name = distro + ".qcow2"
        base_image_vol = self._get_base_image(base_img_name, poolname)

        # parse the base image volume to extract data we'll need to fill in
        # the backing store XML element
        xml_root_node = xmlparser.fromstring(base_image_vol.XMLDesc())
        target_node = xml_root_node.find("target")
        backing_vol_format = target_node.find("format").get("type")
        backing_vol_path = target_node.find("path").text

        # finally create the overlay storage volume
        pool = self.conn.storagePoolLookupByName(provision_poolname)
        volume_xml = template.format(name=volname + ".qcow2", size=size,
                                     backing_vol_path=backing_vol_path,
                                     backing_vol_format=backing_vol_format)
        return pool.createXML(volume_xml)

    def destroy_machine(self, name, ignore_undefined=True):
        try:
            domain = self.conn.lookupByName(name)
        except libvirt.libvirtError as ex:
            if ex.get_error_code() == libvirt.VIR_ERR_NO_DOMAIN and not ignore_undefined:
                raise
            log.debug(f"Not destroying '{name}' because it is already destroyed")
            return

        try:
            domain.destroy()
        except libvirt.libvirtError as ex:
            if ex.get_error_code() == libvirt.VIR_ERR_OPERATION_INVALID:
                log.debug(f"Not destroyng '{name}' because it is already destroyed.")

    def undefine_machine(self, name, ignore_undefined=True):
        try:
            domain = self.conn.lookupByName(name)
        except libvirt.libvirtError as ex:
            if ex.get_error_code() == libvirt.VIR_ERR_NO_DOMAIN and not ignore_undefined:
                raise
            log.debug(f"Not undefying '{name}' because it is already undefined.")
            return

        try:
            domain.undefine()
        except libvirt.libvirtError as ex:
            if ex.get_error_code() == libvirt.VIR_ERR_OPERATION_INVALID:
                log.debug(f"The domain '{name}' is already destroyed.")

    def cleanup_machine(self, name):
        """
        Destroy a libvirt machine.

        This method will look up a VM given a @name and will destroy it.

        :param name: name of the machine to destroy as string

        """

        log.debug(f"Destroying domain '{name}'")

        self.destroy_machine(name)
        self.undefine_machine(name)

    def start_machine(self, name):
        """
        Start a libvirt machine.

        This method will look up a VM given a @name and will start it.

        :param name: name of the machine to start as string

        """

        log.debug(f"starting the VM '{name}'")

        domain = self.conn.lookupByName(name)

        state, reason = domain.state()
        if state == libvirt.VIR_DOMAIN_RUNNING:
            log.debug(f"The VM '{name}' is already running.")
            return
        domain.create()

    def shutdown_machine(self, name, timeout=None):
        """
        Shutdown a libvirt machine.

        This method will look up a VM given a @name and will shut it down.

        :param name: name of the machine to destroy as string
        :param timeout: number of seconds to wait for VM to shutdown

        """

        log.debug(f"Shutting down base VM '{name}'")

        domain = self.conn.lookupByName(name)

        state, reason = domain.state()
        if state == libvirt.VIR_DOMAIN_SHUTOFF:
            log.debug(f"The VM '{name}' is already shutdown.")
            return

        self.wait_for_shutdown(name, timeout)

    def wait_for_shutdown(self, name, timeout, send_shutdown=True):
        from time import sleep

        domain = self.conn.lookupByName(name)

        for i in range(timeout):
            if send_shutdown:
                try:
                    domain.shutdown()
                except libvirt.libvirtError as ex:
                    if "domain is not running" not in ex.get_error_message():
                        raise

            sleep(1)

            state, reason = domain.state()

            log.debug(f"Waiting for shutdown (attempt {i+1}/{timeout})...")

            if state == libvirt.VIR_DOMAIN_SHUTOFF:
                return

        raise Exception(f"Timeout reached for shutdown of VM '{name}'")

    def cleanup_storage(self, name, poolname):
        """
        Clean up overlay storage for a machine.

        The machine '@name' should have been destroyed prior to calling this
        method already.

        :param name: name of the machine storage needs to be cleanup up for as
                     string
        """

        pool_default = self.conn.storagePoolLookupByName(poolname)
        try:
            log.debug(f"Destroying storage for '{name}'")

            volume = pool_default.storageVolLookupByName(name)
            volume.delete()
        except libvirt.libvirtError as ex:
            if ex.get_error_code() != libvirt.VIR_ERR_NO_STORAGE_VOL:
                raise

    def set_target(self, host, target):
        """Inject target OS to host's XML metadata."""

        xml = textwrap.dedent(
            f"""
            <host>
              <target>{target}</target>
            </host>
            """)

        try:
            dom = self.conn.lookupByName(host)
            metadata_flags = libvirt.VIR_DOMAIN_AFFECT_LIVE
            if dom.isPersistent():
                metadata_flags |= libvirt.VIR_DOMAIN_AFFECT_CONFIG
            dom.setMetadata(libvirt.VIR_DOMAIN_METADATA_ELEMENT,
                            xml, "lcitool", LCITOOL_XMLNS,
                            flags=metadata_flags)
        except libvirt.libvirtError as e:
            raise Exception(
                f"Failed to set metadata for '{host}': " + str(e)
            )

    def get_target(self, host):
        dom = self.conn.lookupByName(host)

        try:
            metadata_flags = libvirt.VIR_DOMAIN_AFFECT_CONFIG
            if not dom.isPersistent():
                metadata_flags = libvirt.VIR_DOMAIN_AFFECT_LIVE
            xml = dom.metadata(libvirt.VIR_DOMAIN_METADATA_ELEMENT,
                               LCITOOL_XMLNS,
                               metadata_flags)
        except libvirt.libvirtError as e:
            raise Exception(
                f"Failed to query lcitool metadata for '{dom.name()}': " + str(e)
            )

        xmltree = xmlparser.fromstring(xml)
        target = xmltree.find("target")
        if xmltree.tag != "host" or target is None or target.text is None:
            raise Exception(f"Failed to query lcitool metadata for '{dom.name()}'")

        return target.text

    def get_architecture(self, host):
        dom = self.conn.lookupByName(host)

        xml_root_node = xmlparser.fromstring(dom.XMLDesc())
        os = xml_root_node.find("os")
        if os is None:
            return None
        os_type = os.find("type")
        if os_type is None:
            return None
        return os_type.attrib.get("arch")
